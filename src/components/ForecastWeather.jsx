import React, { Component } from "react";
import axios from "axios";
import {VictoryChart, VictoryLine, VictoryTheme, VictoryLabel} from 'victory';
import {Header, Message} from "semantic-ui-react";


class WeekendComponent extends Component {
    state = {
        actualTemps: [],
        minTemps: [],
        maxTemps: []
    };

    componentDidMount() {
        const populateActual = [];
        const populateMin = [];
        const populateMax = [];

        axios.get("http://localhost:8080/WebService_war/rest/weather/forecast")
            .then(res => {
                res.data.map(combo => {
                    populateActual.push({x: combo.hour, y: combo.actualTemperature});
                    populateMin.push({x: combo.hour, y: combo.minTemperature});
                    populateMax.push({x: combo.hour, y: combo.maxTemperature});


                });
                this.setState({actualTemps: populateActual, minTemps: populateMin, maxTemps: populateMax})
            })


    }

    render() {
        return (<div>

                <Header style={{ textAlign: "center", padding: "30px" }} inverted color='orange' as="h1">
                    Forecast Weather - hourly predictions versus actual temperatures
                </Header>
                <div style={{textAlign: "center", padding:"30px"}}>
                    <Message size='large' color='yellow'>
                        This is a visualization of the actual temperatures for 24.07.2019 compared with the minimum
                        and maximum predicted by Sinoptik.bg</Message>
                </div>
                <Header style={{ textAlign: "center" }} as="h2">
                    <button className="ui blue basic button">Actual Temperature</button>
                    <button className="ui green basic button">Minimum Temperature</button>
                    <button className="ui red basic button">Maximum Temperature</button>
                </Header>

            <VictoryChart width={1400} height={600}
                              theme={VictoryTheme.grayscale}
            >
                <VictoryLine
                    style={{
                        data: {stroke: "#3d7eff"},
                        parent: {border: "1px solid #ccc"},

                    }}
                    labels={(datum) => datum.y}
                    labelComponent={<VictoryLabel renderInPortal dx={10}/>}
                    animate={{
                        duration: 2000,
                        onLoad: {duration: 1300}
                    }}
                    data={
                        this.state.actualTemps
                    }
                />
                <VictoryLine
                    style={{
                        data: {stroke: "#2cf53d"},
                        parent: {border: "1px solid #ccc"},

                    }}
                    labels={(datum) => datum.y}
                    labelComponent={<VictoryLabel renderInPortal dx={10}/>}
                    animate={{
                        duration: 1500,
                        onLoad: {duration: 1300}
                    }}
                    data={
                        this.state.minTemps
                    }
                />
                <VictoryLine
                    style={{
                        data: {stroke: "#ff2b0f"},
                        parent: {border: "1px solid #ccc"},

                    }}
                    labels={(datum) => datum.y}
                    labelComponent={<VictoryLabel renderInPortal dx={10}/>}
                    animate={{
                        duration: 100,
                        onLoad: {duration: 1300}
                    }}
                    data={
                        this.state.maxTemps
                    }
                />


            </VictoryChart>
            </div>
        );
    }
}

export default WeekendComponent;
