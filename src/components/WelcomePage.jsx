import React, { Component } from "react";
import {Message, Icon} from "semantic-ui-react";
class CurrentWeather extends Component {

  render() {
      return (
          <div style={{textAlign: "center", padding:"40px"}}>
            <Message positive>
              <Message.Header>Hey there, fellow jBoxer!</Message.Header>
              <p>
                Please, select an item from <b>the menu</b> to see the weather graphs!
              </p>
            </Message>
          </div>
      )

  }
}

export default CurrentWeather;
