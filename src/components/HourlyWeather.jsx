import React, { Component } from "react";
import axios from "axios";
import {VictoryChart, VictoryLine, VictoryTheme, VictoryLabel} from 'victory';
import {Header, Message} from "semantic-ui-react";
class HourlyComponent extends Component {
  state = {
      weatherArray:[]
  };

  componentDidMount() {
      const populateArray = [];

      axios.get("http://localhost:8080/WebService_war/rest/weather/hourly")
          .then(res => {
              res.data.map(combo=>{
                  populateArray.push({ x:combo.hour, y:combo.temperature});


              });
            this.setState({weatherArray:populateArray})
          })


}
  render() {
      console.log( "THIS STATTE", this.state );
    return (<div>
        <Header style={{ textAlign: "center", padding: "30px" }}  inverted color='orange' as="h1">
            Hourly Temperatures
        </Header>
            <div style={{textAlign: "center", padding:"30px"}}>
            <Message size='large' color='yellow'>
                This is a visualization of the hourly temperatures for 24.07.2019 from Sinoptik.bg.</Message>
            </div>
            <VictoryChart width={1400} height={600}
            theme={VictoryTheme.material}
        >
          <VictoryLine
              style={{
                data: { fill: "#35e898" },
                parent: { border: "1px solid #ccc"},

              }}
              labels={(datum) => datum.y}

              labelComponent={<VictoryLabel renderInPortal dx={10}/>}
              animate={{
                  duration: 2000,
                  onLoad: { duration: 1300 }
              }}
              data={
                  this.state.weatherArray
              }
          />
        </VictoryChart>
        </div>
    );
  }
}

export default HourlyComponent;
