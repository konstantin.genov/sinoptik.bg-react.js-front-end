import React, { Component } from "react";
import { Route, Switch, Redirect, withRouter } from "react-router-dom";
import Layout from "./wrappers/Layout";
import ForecastFiveDay from "./components/ForecastFiveDay";
import HourlyWeather from "./components/HourlyWeather";
import ForecastWeather from "./components/ForecastWeather"
import WelcomePage from "./components/WelcomePage";

class App extends Component {
  state = {
    redirect: false
  };

  renderAuthSwitch = () =>
  {
    return (
      <Switch>
        <Route path="/hourly" component={HourlyWeather} />
        <Route path="/forecastfive" component={ForecastFiveDay} />
        <Route path="/forecast" component={ForecastWeather} />
        <Route path="/" component={WelcomePage} />
        <Redirect to="/" />
      </Switch>
    );
  };
  render() {
    return <Layout>{this.renderAuthSwitch()}</Layout>;
  }
}

export default withRouter(App);
