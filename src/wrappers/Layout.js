import React from "react";
import { Header, Divider, Segment, Image } from "semantic-ui-react";
import MyMenu from "./MyMenu";

export default props => {
    return (
        // the container is required to use multiple components with children
        // the link in the head loads the whole css for the semantic components

        <div >
            <div style={{ padding: "0px" }}>
                <Segment inverted>
                <Header style={{ textAlign: "center" }} inverted color='white' as="h2">
                    <Image size='small' src={('https://jboxers.com/wp-content/uploads/2018/02/JB-logo-Inter-50px.png')} />
                    jBoxers Weather Project
                </Header>
                </Segment>

            </div>
            <MyMenu/>
            <Divider hidden />
            {props.children}
        </div>

        //the children elements are any elements between the opening and closing tag of our components
    ); // exmaple <p className="blabla">this is a child</p>
};