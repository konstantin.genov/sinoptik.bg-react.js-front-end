import React, { Component } from "react";
import { Menu, Segment } from "semantic-ui-react";
import { NavLink } from "react-router-dom";

class MyMenu extends Component {
  state = {
    activeItem: null
  };

  handleItemClick = (e, { name }) => this.setState({ activeItem: name });

  renderAfterAuthButton() {
    return (
        <Segment inverted textAlign="center" floated>
            <Menu inverted secondary compact >
            <Menu.Item
          as={NavLink}
          to="/hourly"
          name="HourlyWeather"
          active={this.state.activeItem === "HourlyWeather"}
          onClick={this.handleItemClick}
        />
                <Menu.Item
          floated={"right"}
          as={NavLink}
          to="/forecast"
          name="Forecast - Hourly versus Actual"
          active={this.state.activeItem === "ForecastWeather"}
          onClick={this.handleItemClick}
        />
                <Menu.Item
                    floated={"right"}
                    as={NavLink}
                    to="/forecastfive"
                    name="Forecast Five Day versus Actual"
                    active={this.state.activeItem === "Forecast Five Day"}
                    onClick={this.handleItemClick}
                />
            </Menu>
        </Segment>
    );
  }

  render() {
    return <>{this.renderAfterAuthButton()}</>;
  }
}

export default MyMenu;
